from django.apps import AppConfig


class ConversationsConfig(AppConfig):
    name = 'pushtogether.conversations'
    verbose_name = "Conversations"
